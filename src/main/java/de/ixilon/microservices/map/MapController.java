// Copyright (C) 2019 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.microservices.map;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import de.ixilon.terrarium.TerrariumValue;
import de.ixilon.wms.MapRequest;
import de.ixilon.wms.MediaResponse;
import de.ixilon.wms.MetadataRequest;
import de.ixilon.wms.WebMapService;
import de.ixilon.wms.exception.ServiceException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.opengis.wms.Layer;

/**
 * This controller defines the REST API endpoints.
 */
@RestController
public class MapController {

  private static final String ETAG = System.getenv("CI_BUILD_REF");
  private static final long LAST_MODIFIED = Instant.now().toEpochMilli();
  private static final String CACHE_CONTROL_HEADER =
      CacheControl.maxAge(1, TimeUnit.DAYS).cachePublic().mustRevalidate().getHeaderValue();

  private final WebMapService service;

  @Autowired
  public MapController(WebMapService service) {
    this.service = service;
  }

  @ApiOperation(value = "List maps.", response = Iterable.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully retrieved list"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource is forbidden") })
  @RequestMapping(value = "/maps", method = RequestMethod.GET, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Iterable<String> listMaps() throws ServiceException {
    ImmutableSet.Builder<String> result = ImmutableSet.builder();
    for (String layer : getLayerNames()) {
      String[] parts = layer.split(":");
      if (parts.length == 2) {
        result.add(parts[0]);
      }
    }
    return result.build();
  }

  @ApiOperation(value = "List layers of a maps.", response = Iterable.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully retrieved list"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource is forbidden") })
  @RequestMapping(value = "/maps/{map}/layers", method = RequestMethod.GET, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public Iterable<String> listLayers(@PathVariable(name = "map") String map)
      throws ServiceException {
    ImmutableSet.Builder<String> result = ImmutableSet.builder();
    for (String layer : getLayerNames()) {
      String[] parts = layer.split(":");
      if (parts.length == 2 && map.equals(parts[0])) {
        result.add(parts[1]);
      }
    }
    return result.build();
  }

  private ImmutableList<String> getLayerNames() throws ServiceException {
    ImmutableList.Builder<String> result = ImmutableList.builder();
    for (Layer layer : service.getCapabilities(new MetadataRequest())
        .getCapability().getLayer().getLayer()) {
      result.add(layer.getName());
    }
    return result.build();
  }

  @ApiOperation(
      value = "Get a regular grid of width x height byte samples of a grayscale map."
          + "The red pixel value is used for color maps."
          + "The grid is limited by a bounding box of spherical (lat,lon) coordinates",
      response = Iterable.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully retrieved list"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource is forbidden") })
  @RequestMapping(value = "/maps/{map}/layers/{layer}/bytes", method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_OCTET_STREAM_VALUE})
  public ResponseEntity<byte[]> getByteSamples(
      @PathVariable(name = "map") String map,
      @PathVariable(name = "layer") String layer,
      @RequestParam(name = "minx") double minx,
      @RequestParam(name = "miny") double miny,
      @RequestParam(name = "maxx") double maxx,
      @RequestParam(name = "maxy") double maxy,
      @RequestParam(name = "width") int width,
      @RequestParam(name = "height") int height,
      WebRequest webRequest) throws IOException {
    if (webRequest.checkNotModified(ETAG, LAST_MODIFIED)) {
      return null;
    }
    MapRequest request = new MapRequest()
        .setFormat(MediaType.IMAGE_PNG_VALUE)
        .setWidth(width)
        .setHeight(height)
        .setCrs("EPSG:4326")
        .setBbox(minx, miny, maxx, maxy)
        .setLayers(map + ":" + layer)
        .setStyles("");
    MediaResponse response = service.getMap(request);
    BufferedImage image = ImageIO.read(response.getInputStream());
    
    return ResponseEntity.ok()
        .header(HttpHeaders.CACHE_CONTROL, CACHE_CONTROL_HEADER)
        .body(toByteArray(image));
  }

  private static byte[] toByteArray(BufferedImage image) {
    int width = image.getWidth();
    int height = image.getHeight();
    byte[] result = new byte[height * width];
    int index = 0;

    for (int row = 0; row < height; row++) {
      for (int col = 0; col < width; col++) {
        Color pixel = new Color(image.getRGB(col, row));
        result[index++] = (byte) pixel.getRed();
      }
    }

    return result;
  }

  @ApiOperation(
      value = "Get a regular grid of width x height float samples of a Terrarium encoded color map."
          + "The grid is limited by a bounding box of spherical (lat,lon) coordinates",
      response = Iterable.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully retrieved list"),
      @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
      @ApiResponse(code = 403, message = "Accessing the resource is forbidden") })
  @RequestMapping(value = "/maps/{map}/layers/{layer}/floats", method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_OCTET_STREAM_VALUE})
  public ResponseEntity<byte[]> getFloatSamples(
      @PathVariable(name = "map") String map,
      @PathVariable(name = "layer") String layer,
      @RequestParam(name = "minx") double minx,
      @RequestParam(name = "miny") double miny,
      @RequestParam(name = "maxx") double maxx,
      @RequestParam(name = "maxy") double maxy,
      @RequestParam(name = "width") int width,
      @RequestParam(name = "height") int height,
      WebRequest webRequest) throws IOException {
    if (webRequest.checkNotModified(ETAG, LAST_MODIFIED)) {
      return null;
    }
    MapRequest request = new MapRequest()
        .setFormat(MediaType.IMAGE_PNG_VALUE)
        .setWidth(width)
        .setHeight(height)
        .setCrs("EPSG:4326")
        .setBbox(minx, miny, maxx, maxy)
        .setLayers(map + ":" + layer)
        .setStyles("");
    MediaResponse response = service.getMap(request);
    BufferedImage image = ImageIO.read(response.getInputStream());
    ByteBuffer byteBuffer = ByteBuffer.allocate(image.getWidth() * image.getHeight() * Float.BYTES);
    int index = 0;
    
    for (int y = 0; y < image.getHeight(); y++) {
      for (int x = 0; x < image.getWidth(); x++) {
        Color color = new Color(image.getRGB(x, y));
        TerrariumValue value =
            new TerrariumValue(color.getRed(), color.getGreen(), color.getBlue());

        byteBuffer.putFloat(index, value.toFloat());
        index += Float.BYTES;
      }
    }
    
    return ResponseEntity.ok()
        .header(HttpHeaders.CACHE_CONTROL, CACHE_CONTROL_HEADER)
        .body(byteBuffer.array());
  }
}
