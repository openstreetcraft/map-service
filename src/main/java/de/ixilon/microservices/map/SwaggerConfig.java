// Copyright (C) 2019 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.microservices.map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

  /**
   * Configures the swagger-springmvc framework.
   */
  @Bean
  public Docket productApi() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.basePackage("de.ixilon.microservices.map"))
        .build()
        .apiInfo(apiInfo());
  }

  private static ApiInfo apiInfo() {
    return new ApiInfoBuilder()
        .title("Map REST API")
        .description("Map REST API for Openstreetcraft")
        .version("1.0")
        .termsOfServiceUrl("https://openstreetcraft.net/terms")
        .contact(new Contact("Openstreetcraft", "https://openstreetcraft.net", "support@openstreetcraft.net"))
        .license("AGPL Version 3.0")
        .licenseUrl("https://www.gnu.org/licenses/agpl-3.0.html")
        .build();
  }

}
