// Copyright (C) 2019 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.microservices.map;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import de.ixilon.wms.MetadataRequest;
import de.ixilon.wms.WebMapService;
import de.ixilon.wms.exception.ServiceException;
import net.opengis.wms.Capability;
import net.opengis.wms.Layer;
import net.opengis.wms.WMSCapabilities;

public class MapControllerTest {

  private MapController controller;
  
  @Mock
  private WebMapService service;

  @Mock
  private WMSCapabilities capabilities;

  @Mock
  private Capability capability;

  @Mock
  private Layer rootLayer;

  @Before
  public void setUp() throws ServiceException {
    MockitoAnnotations.initMocks(this);
    
    Mockito.when(service.getCapabilities(Mockito.any(MetadataRequest.class)))
        .thenReturn(capabilities);
    Mockito.when(capabilities.getCapability()).thenReturn(capability);
    Mockito.when(capability.getLayer()).thenReturn(rootLayer);
    
    controller = new MapController(service);
  }

  @Test
  public void listMaps() throws ServiceException {
    Mockito.when(rootLayer.getLayer()).thenReturn(ImmutableList.of(
        layer("foo"),
        layer("bar:baz"),
        layer("bar:qux")));
    Iterable<String> actuals = controller.listMaps();
    Iterable<String> expecteds = ImmutableSet.of("bar");
    assertEquals(expecteds, actuals);
  }

  @Test
  public void listLayers() throws ServiceException {
    Mockito.when(rootLayer.getLayer()).thenReturn(ImmutableList.of(
        layer("foo"),
        layer("bar:baz"),
        layer("bar:qux")));
    Iterable<String> actuals = controller.listLayers("bar");
    Iterable<String> expecteds = ImmutableSet.of("baz", "qux");
    assertEquals(expecteds, actuals);
  }

  private Layer layer(String name) {
    Layer layer = new Layer();
    layer.setName(name);
    return layer;
  }

}
