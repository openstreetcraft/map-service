# Overview

Provides geographical information at a given spherical location on the map.

# Software Design

* RESTful webservice as a facade to access a WMS server

The WMS server must export WMS layers using the syntax `foo:bar`.
The colon separates map ("foo") and layer ("bar").
Maps are corresponding to Minecraft maps (like "EARTH") and
layers are value classes (like "BIOME", "ELEVATION").

WMS media response is 8-Bit grayscale PNG and
HTTP content type is "application/octet-stream" with data values of type `byte[]` or `float[]`.

# Configuration
 
The following WMS server is configured per default:

```
http://mapproxy/service
```

This URL can be changed by the environment variable `WMS_URL`.

# Runtime

Service can be started with

```
./gradlew distDocker
docker-compose up --build map
```

# REST API

REST API of a running service can be discovered with Swagger UI: 

http://localhost:8080/api/v1/swagger-ui.html

# Links

The project [openstreetcraft-api](https://gitlab.com/openstreetcraft/openstreetcraft-api)
is a Java library for easy access to this service.
